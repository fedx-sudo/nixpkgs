{ lib
, fetchFromGitHub
, stdenv
, makeWrapper
, qemu
, gnugrep
, lsb-release
, jq
, procps
, python3
, cdrtools
, usbutils
, util-linux
, spicy
, swtpm
, wget
, xdg-user-dirs
, xrandr
, zsync
, OVMFFull
#, OVFM-secureBoot
}:
let
  runtimePaths = [
    qemu
    gnugrep
    jq
    lsb-release
    procps
    python3
    cdrtools
    usbutils
    util-linux
    spicy
    swtpm
    wget
    xdg-user-dirs
    xrandr
    zsync
    #OVMFFull
    #OVFM-secureBoot
  ];
in

stdenv.mkDerivation rec {
  pname = "quickemu";
  version = "2.3.0";

  src = fetchFromGitHub {
    owner = "wimpysworld";
    repo = pname;
    rev = version;
    sha256 = "sha256-q5hwxCkL15HcEXTlUYbSn909NBnnx5VE/9PcTMuaEo4=";
  };

  nativeBuildInputs = [ makeWrapper ];
  patches = ./nixos.patch;
  installPhase = ''
    runHook preInstall

   

    install -Dm755 -t "$out/bin" quickemu quickget macrecovery

   for f in quickget macrecovery quickemu; do
    wrapProgram $out/bin/$f --prefix PATH : "${lib.makeBinPath runtimePaths}"
   done

    runHook postInstall
  '';

  meta = with lib; {
    description = "Quickly create and run optimised Windows, macOS and Linux desktop virtual machines";
    homepage = "https://github.com/wimpysworld/quickemu";
    license = licenses.mit;
    maintainers = with maintainers; [ fedx-sudo ];
  };
}
